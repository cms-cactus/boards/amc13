#!/bin/bash
#
AMC13Tool2.exe -c ${1}/c -X init.amc13
AMC13Tool2.exe -c ${1}/c -X trig.amc13
sleep 0.1
echo before
AMC13Tool2.exe -c ${1}/c -X read_status_wildcard.amc13 > $2_pre_status.txt
echo Trigger
AMC13Tool2.exe -c ${1}/c -X trig.amc13
echo Sleep
sleep 0.1
echo After
AMC13Tool2.exe -c ${1}/c -X read_status_wildcard.amc13 > $2_post_status.txt
echo "Creating $2_diffs.txt"
diff --width=150 -y $2_pre_status.txt $2_post_status.txt  | fgrep \| > $2_diffs.txt
