#include <amc13/AMC13.hh>

#include <stdlib.h>  //for abort
#include <stdio.h> //printf
#include <iomanip> //std::setw;std::setfill();


namespace amc13{


void AMC13::LoadDefaultDebugEntries(){
  AppendDebugEntry(T1,0x0,0x10ff);
  AppendDebugEntry(T2,0x0,0xff);
}

bool cmp_entry(DumpDebugEntry entry1, DumpDebugEntry entry2){
  if(entry1.board==entry2.board) return (entry1.address<entry2.address);
  else return (entry1.board>entry2.board);
}

void AMC13::AppendDebugEntry(AMC13::Board board, uint32_t address, uint32_t count){
  DumpDebugEntry temp;
  if(!((T2 == board) || (T1 == board))){
    //invalid board type;
    //throw exeception!
     amc13::Exception::BadChip e;
     e.Append( "AMC13::AppendDebugEntry() - Given vector binaryDump has incorrect format");
     throw e;
  }
  temp.board = int8_t(board);
  temp.address = address;
  temp.count = count;
  //Append temp structure to readout list
  DumpDebugList.push_back(temp);
  std::sort(DumpDebugList.begin(),DumpDebugList.end(),cmp_entry);
}

void AMC13::Readout(std::vector<uint32_t> & binaryDump){
  size_t wordCount = sizeof(DumpDebugCapture)/sizeof(uint32_t); // Sentinel value, block count, word_count
  //Compute the size of all the blocks
  for(size_t iEntry = 0; iEntry < DumpDebugList.size();iEntry++){
    
    wordCount += (sizeof(DumpDebugBlock)/sizeof(uint32_t) +          //Struct
		  DumpDebugList[iEntry].count);     //Variable data in struct
  }

  //Allocate data
  binaryDump.resize(wordCount,0xffffffff);

  DumpDebugCapture * capture = (DumpDebugCapture*) &(binaryDump[0]);
  //Set capture struct internals
  capture->magic      = 0xdeadcafe;
  capture->format     = 1;
  capture->blockCount = DumpDebugList.size();
  capture->wordCount  = wordCount;
  
  //  uint32_t * dataPointer = &(binaryDump[0]) + sizeof(capture)/sizeof(uint32_t); //Move to the start of the block data
  uint32_t * dataPointer = capture->blockData;
  for(size_t iEntry = 0; iEntry < DumpDebugList.size();iEntry++){
    //Readout registers and fill in block struct
    ReadDebugEntry(DumpDebugList[iEntry],dataPointer);
    //Move the dataPointer to the next block;
    dataPointer += sizeof(DumpDebugBlock)/sizeof(uint32_t); //block struct
    dataPointer += DumpDebugList[iEntry].count; //block data
  }
}

void AMC13::ReadDebugEntry(DumpDebugEntry const & entry,uint32_t * binaryDump){
  //cast the pointer to a block pointer.
  DumpDebugBlock * block = (DumpDebugBlock * ) binaryDump;
  block->datasize = entry.count;
  block->board     = uint32_t(entry.board);
  block->address   = entry.address;

  //do the reading here
  //one line function to fill the array
  read(Board(block->board), block->address, block->datasize, &(block->data[0]));
}

uint32_t * AMC13::readblock(uint32_t* datapointer, std::ostream& output, uint32_t* binaryDumpStart, uint32_t wordCount){
   //Print out this datablock and return the pointer of the next block
   //check segment violation
   DumpDebugBlock * block = (DumpDebugBlock*) datapointer;
   if(datapointer + 3 + block->datasize > binaryDumpStart + wordCount) {
     amc13::Exception::UnexpectedRange e;
     e.Append( "AMC13::readblock() - specified size of the block exceeded data range");
     throw e;
   }
;
   //--------Output a header---------
   output<<"--- ";
   if(block->board==1) output<<"T1 Registers -- ";else output<<"T2 Registers --";
   output<<" Starting from "<<"0x" << std::hex << std::setfill('0') << std::setw(8) << block->address;
   output<<" --- block size (in number of uint32 words) : "<< std::dec <<  block->datasize << " ---";
   //--------Print out the data, 8 uint32_t per line---------
   for(size_t iEntry=0; iEntry < block->datasize; iEntry++){
	   if(iEntry%8==0) output<<std::endl<< std::hex << std::setfill('0') << std::setw(8) << block->address+iEntry <<":  ";
	   output<<" "<<std::hex<<std::setfill('0')<<std::setw(8)<<block->data[iEntry];
   }
   output<<std::endl;
   return &(block->data[0])+block->datasize;
}


void AMC13::PrintDump(const std::vector<uint32_t>& binaryDump, std::ostream& output){	
  //Take the vector and decode it into readable format
  if(binaryDump.size()<4) {//Check the minimal lenghth of the input vector
     amc13::Exception::UnexpectedRange e;
     e.Append( "AMC13::PrintDump() - Given vector binaryDump is smaller than the expected smallest size");
     throw e;
  }
  DumpDebugCapture * capture = (DumpDebugCapture*) &(binaryDump[0]);
  if(!(capture->format==1 and capture->magic==0xDEADCAFE)) {//Check the format of the input vector
     amc13::Exception::BadValue e;
     e.Append( "AMC13::PrintDump() - Given vector binaryDump has incorrect format");
     throw e;
  }
  if(capture->wordCount != binaryDump.size()) {
  amc13::Exception::BadValue e;
  e.Append( "AMC13::PrintDump() - Specified size of binaryDump doesn't match the actual size");
  throw e;
  }
  uint32_t * datapointer = capture->blockData;
  while(datapointer + 3/*minimal size of the block*/ - &(binaryDump[0]) < capture->wordCount) 
	  datapointer=readblock(datapointer, output,(uint32_t*) binaryDump.data(), capture->wordCount);
}


}//End of namespace amc13
