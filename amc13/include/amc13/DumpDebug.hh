#ifndef __STRUCT_HH__
#define __STRUCT_HH__

#include <vector> 
#include <stdint.h> //uintXX_t types
#include <iostream> //stream as output

//Should sort it first by board then by address-----------
struct DumpDebugEntry{
  int8_t board;
  uint32_t address;
  uint32_t count;
};


struct DumpDebugBlock{
  uint32_t datasize;//length of the data[]
  uint32_t board;
  uint32_t address;
  uint32_t data[];
};

struct DumpDebugCapture{
  uint32_t magic;
  uint32_t format;
  uint32_t blockCount;
  uint32_t wordCount; //length of this struct
  uint32_t blockData[]; //in place structs
};

#endif
