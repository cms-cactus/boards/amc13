BUILD_HOME=${AMC13_ROOT}
$(info Using AMC13_ROOT=${AMC13_ROOT})
$(info Using BUILD_HOME=${BUILD_HOME})

# Cactus config
CACTUS_ROOT ?= /opt/cactus

# RPM generics
RPM_OS = $(patsubst %.cern,%,$(shell rpm --eval "%{dist}" | cut -c2-))

# Compilers
CPP:=g++
LD:=g++

# Tools
MakeDir=mkdir -p

# Python
PYTHON ?= python3
PYTHON_VERSION ?= $(shell ${PYTHON} -c "import distutils.sysconfig; print(distutils.sysconfig.get_python_version())")
PYTHON_INCLUDE_PREFIX ?= $(shell ${PYTHON} -c "import distutils.sysconfig; print(distutils.sysconfig.get_python_inc())")
PYTHON_LIB_PREFIX ?= $(shell ${PYTHON} -c "from distutils.sysconfig import get_python_lib; import os.path; print(os.path.split(get_python_lib(standard_lib=True))[0])")

ifndef DEBUG
# Compiler flags
CxxFlags = -g -Wall -O3 -MMD -MP -fPIC -std=c++0x
LinkFlags = -g -shared -fPIC -Wall -O3
ExecutableLinkFlags = -g -Wall -O3
else
CxxFlags = -g -ggdb -Wall -MMD -MP -fPIC -std=c++0x
LinkFlags = -g -ggdb -shared -fPIC -Wall
ExecutableLinkFlags = -g -ggdb -Wall
endif
