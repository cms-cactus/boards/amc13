//
// class to keep track of GLIB modules for command-line tool
// (possibly useful for other purposes)
//
 
#include <map>
#include <string>
#include <vector>
#include <fstream>
#include <stdint.h>

#include "uhal/uhal.hpp"

class Module {
    
public:
  Module();
  ~Module();
  //    void Connect( const std::string file, const std::string addressTablePath, const std::string prefix = "");
  void Connect( const std::string file, const std::string addressTableFile);
  std::string Show();
    
  uhal::HwInterface *hw;
  std::string connectionFile;

  std::ofstream& getStream(); 
  bool isFileOpen() { return stream != NULL;}
  void setStream(const char* file);
  void closeStream();
  std::string getFileName() { return fileName; }
  void hostnameToIp(const char *hostname, char *ip);

private :
  std::ofstream* stream;
  std::string fileName;
};

