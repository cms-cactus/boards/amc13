//
// Debug pixel xDAQ code
//

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <stdlib.h>

#include <time.h>

#include "PixelAMC13Interface.h"

#include "amc13/AMC13.hh"

int main(int argc, char* argv[])
{


  //--- inhibit most noise from uHAL
  uhal::setLogLevelTo(uhal::Error());

  // URI for S/N 86 with control hub
  // NOTE:  address tables must be in /opt/cactus/etc/amc13
  printf("constructor...\n");
  PixelAMC13Interface* pAMC13 = new PixelAMC13Interface(  "chtcp-2.0://localhost:10203?target=192.168.1.83:50001",
							  "chtcp-2.0://localhost:10203?target=192.168.1.82:50001"
							  );
  amc13::AMC13* amc13 = pAMC13->Get(); // get direct AMC13 ptr

  pAMC13->SetDebugPrints( true);

  pAMC13->SetNewWay( true);
  pAMC13->SetMask( "1");

  printf("configure...\n");
  pAMC13->Configure();

  amc13->startRun();		// have to do this somewhere!

  int l1a_0 = amc13->read( amc13::AMC13Simple::T1, "STATUS.GENERAL.L1A_COUNT_LO");

  //  for( int i=0; i<5; i++) {
    //    printf("LevelOne()...\n");
    //    pAMC13->LevelOne();
    printf("CalSync()...\n");
    pAMC13->CalSync();
    //  }

    int l1a_1 = amc13->read( amc13::AMC13Simple::T1, "STATUS.GENERAL.L1A_COUNT_LO");  

  printf("Triggers %d %d\n", l1a_0, l1a_1);

}

