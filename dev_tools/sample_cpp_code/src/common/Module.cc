#include "Module.hh"

#include <boost/regex.hpp>

//For networking constants and structs
#include <sys/types.h>  
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h> //for inet_ntoa


Module::Module() {
  hw = NULL;
  connectionFile = "??";
  stream = NULL;
  fileName = "";
}
  
  
Module::~Module() {
  if( hw != NULL) {
    delete hw;
  }
  if (stream != NULL) {
    stream->close();
    delete stream;
  }
}
  
  
// connect to module
// Expects IP address only (for now) and address table name with absolute path
// IP address for now must be numeric, and may have suffix "/c"
// to force use of controlhub
//
void Module::Connect( const std::string file, const std::string addressTableName) {
  connectionFile = file;

  // check for numeric IP address using Boost
  // separate last octet so we can increment it
  static const boost::regex regIP("(\\d{1,3}.\\d{1,3}.\\d{1,3}.)(\\d{1,3})(/[cC])?");
  static const boost::regex fileXml("\\S*\\.[xX][mM][lL]");
  static boost::cmatch what;
  static char t1_uri[256];

  if( boost::regex_match( file.c_str(), what, regIP)) {
    std::string ip_addr(what[1].first, what[1].second); // extract 1st 3 octets
    std::string ip_last(what[2].first, what[2].second); // extract last octet
    uint8_t oct_last = atoi( ip_last.c_str());
    bool use_ch = what[3].matched; // check for /c suffix
    if( use_ch) {
      printf("use_ch true\n");
    }
    else
      printf("use_ch false\n");

    // specify protocol prefix
    std::string proto = use_ch ? "chtcp-2.0://localhost:10203?target=" : "ipbusudp-2.0://";

    snprintf( t1_uri, 255, "%s%s%d:50001", proto.c_str(), ip_addr.c_str(), oct_last);      
      
    printf("Created URI from IP address:\n  %s\n", t1_uri);

    std::string addrTableFull = "file://" + addressTableName;

    printf("Address table name is %s\n", addrTableFull.c_str());

    try {
      hw = new uhal::HwInterface( uhal::ConnectionManager::getDevice("GLIB", t1_uri, addrTableFull));
    } catch( uhal::exception::exception& e) {
      e.append("Module::Connect() creating hardware device");
      printf("Error creating uHAL hardware device\n");
    }
  } else {
    printf("Connection string \"%s\" not recognized as IP address\n", file.c_str());
  }

}
  
std::string Module::Show() {
  char buff[80];
  snprintf( buff, 80, "IP: %s", connectionFile.c_str() );
  std::string str(buff);
  return str;
}

//If there is no file open, return std::cout
std::ofstream& Module::getStream() {
  return *stream;
}

//If there is a file currently open, it closes it
void Module::setStream(const char* file) {
  if (stream != NULL) {
    stream->close();
    delete stream;
    stream = NULL;
  }
  stream = new std::ofstream;
  stream->open(file);
  fileName = file;
}

//Closes the stream
void Module::closeStream() {
  if (stream != NULL) {
    stream->close();
    delete stream;
    stream = NULL;
    fileName = "";
  }
}

// get ip from domain name
// ATTENTION should probably be void with throws instead of return 1 for failure, 0 for success
void Module::hostnameToIp( const char *hostname, char *ip) { //change hostname to const str
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_in *h;
  int rv;
    
  printf("memset hints\n");
  memset(&hints, 0, sizeof hints);
  printf("memset hints complete\n");
  hints.ai_family = AF_INET; // use AF_INET assuming IPv4 or AF_UNSPEC if also accept IPv6 
  hints.ai_socktype = SOCK_STREAM;
    
  if ( (rv = getaddrinfo( hostname , NULL , &hints , &servinfo) ) != 0) {
    // ATTENTION: should throw exception and append gai_strerror(rv), print and return 1 placeholder for now
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv)); 
    return;
  }
  printf("start loop\n");
  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    h = (struct sockaddr_in *) p->ai_addr;
    printf("strcpy\n");
    //ATTENTION: Should check that ip is large enough to copy given IP address
    strcpy(ip , inet_ntoa( h->sin_addr ) );
  }
  printf("free\n");
  freeaddrinfo(servinfo); // all done with this structure
}
