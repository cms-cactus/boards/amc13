
#include "Launcher.hh"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
// for usleep() in local trigger gen
#include <unistd.h>

// split a string on a delimiter
//
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

int Launcher::Connect(std::vector<std::string> strArg,
		      std::vector<uint64_t> intArg)
{
  //Check for a filename
  if(strArg.size() ==  0) {
    printf("Connect: Missing IP address.\n");
    return 0;
  }
    
  //create GLIB module
  Module* mod = new Module();
  mod->Connect( strArg[0], GetAddressTablePath());
  GLIBModule.push_back( mod);
    
  return 0;
}
  

int Launcher::Select(std::vector<std::string> strArg,
			  std::vector<uint64_t> intArg) {
  if(intArg.size() == 0) {
    printf("GLIBSelect: Missing GLIB number\n");
    return 0;
  }
  if(intArg[0] >= GLIBModule.size()) {
    printf("GLIBSelect: Bad GLIB number\n");
    return 0;
  }
  defaultGLIBno = intArg[0];
  printf("Setting default GLIB to %zu\n",defaultGLIBno);
  return 0;
}
